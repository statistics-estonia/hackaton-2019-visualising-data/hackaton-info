# Hackathon 2019 – Visualising Data

[General guidelines](General guidelines.pdf)

[Visualization ideas](Visualization ideas.pdf)

[MFA data.xlsx](MFA data.xlsx)

[Youth_in_Estonia.xlsx](Youth_in_Estonia.xlsx)

[Birthdays 2019.xlsx](Birthdays 2019.xlsx)

[Additional info and registration](https://www.garage48.org/events/garage48-visualising-data)